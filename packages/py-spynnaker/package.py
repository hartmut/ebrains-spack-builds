# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PySpynnaker(PythonPackage):
    """This package provides common code for PyNN implementations for
    SpiNNaker."""

    homepage = "https://github.com/SpiNNakerManchester/sPyNNaker"
    pypi = "sPyNNaker/sPyNNaker-1!7.0.0.tar.gz"

    maintainers = ["rowley"]

    version("7.0.0", sha256="caeaa624e3fdbca3b938c9be7ea4c78a51a037e659389fb01952822f069664db")

    depends_on("python@3.7:", type=("build", "run"))
    depends_on("py-spinnfrontendcommon@7.0.0", type=("build", "run"))
    depends_on("py-matplotlib", type=("build", "run"))
    depends_on("py-quantities", type=("build", "run"))
    depends_on("py-pynn", type=("build", "run"))
    depends_on("py-neo", type=("build", "run"))
    depends_on("py-lazyarray", type=("build", "run"))
    depends_on("py-scipy", type=("build", "run"))
    depends_on("py-csa", type=("build", "run"))
