# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyDash(PythonPackage):
    """Python framework for building ML & data science web apps"""

    homepage = "https://dash.plotly.com/"
    pypi = "dash/dash-2.16.1.tar.gz"

    version("2.16.1", sha256="b2871d6b8d4c9dfd0a64f89f22d001c93292910b41d92d9ff2bb424a28283976")
    version("2.15.0", sha256="d38891337fc855d5673f75e5346354daa063c4ff45a8a6a21f25e858fcae41c2")

    depends_on("python@3.8:", type=("build", "run"))
    depends_on("py-setuptools", type="build")
    
    depends_on("py-flask@1.0.4:3.0", type=("build", "run"))
    depends_on("py-werkzeug@:3.0", type=("build", "run"))
    depends_on("py-plotly@5:", type=("build", "run"))
    depends_on("py-importlib-metadata", type=("build", "run"))
    depends_on("py-typing-extensions@4.1.1:", type=("build", "run"))
    depends_on("py-requests", type=("build", "run"))
    depends_on("py-retrying", type=("build", "run"))
    depends_on("py-nest-asyncio", type=("build", "run"))

