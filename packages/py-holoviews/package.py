# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyHoloviews(PythonPackage):
    """HoloViews is an open-source Python library designed to make data analysis and visualization seamless and simple."""

    homepage = "https://holoviews.org/"
    pypi = "holoviews/holoviews-1.18.0.tar.gz"

    version("1.17.1", sha256="ca30c661508b22e9e8c119dbc3e4a5d851987b43c30565db180b291d8fd770a4")


    depends_on('py-setuptools', type='build')
    depends_on('py-pyct', type='build')
    depends_on('py-matplotlib', type=('build','run'))
    depends_on('py-bokeh', type=('build','run'))
    depends_on('py-param@1.12.0:3.0', type=('build','run'))
    depends_on('py-numpy@1.0:', type=('build','run'))
    depends_on('py-pyviz-comms@0.7.4:', type=('build','run'))
    depends_on('py-pandas@0.20.0:', type=('build','run'))
    depends_on('py-packaging', type=('build','run'))
    depends_on('py-panel', type=('build','run'))
    depends_on('py-colorcet', type=('build','run'))
    depends_on('py-jupyterlab-widgets', type='run')
    depends_on('py-ipywidgets', type='run')
