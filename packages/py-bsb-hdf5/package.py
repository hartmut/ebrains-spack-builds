# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyBsbHdf5(PythonPackage):
    """An HDF-5 based storage engine for the BSB framework."""

    homepage = "https://github.com/dbbs-lab/bsb-hdf5"
    url = "https://pypi.org/packages/py3/b/bsb_hdf5/bsb_hdf5-0.8.3-py3-none-any.whl"

    maintainers = ["helveg"]

    version('0.8.3', sha256="38162bfe9470b87cb30a2bff78dce68fc1b97f2df7d7e3b288c16b671f7579e5", expand=False)

    depends_on("py-setuptools", type="build")
    # depends_on("py-bsb@4.0.0a57:")
    depends_on("py-shortuuid")
    depends_on("py-h5py@3.0:")
